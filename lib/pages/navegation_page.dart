import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class NavigationPage extends StatelessWidget {
  const NavigationPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) =>  _NotificationModel(),
      child: Scaffold(
        appBar: AppBar(
            centerTitle: true,
            backgroundColor: Colors.pink,
            title: Text('Notifications Page')),
        floatingActionButton: _BotonFlotante(),
        bottomNavigationBar: _BottomNavigationBar(),
      ),
    );
  }
}

class _BottomNavigationBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final int numberNotify =  Provider.of<_NotificationModel>(context).number;
    return BottomNavigationBar(
      currentIndex: 0,
      selectedItemColor: Colors.pink,
      items: [
        BottomNavigationBarItem(
            label: 'Bones', icon: FaIcon(FontAwesomeIcons.bone)),
        BottomNavigationBarItem(
            label: 'Notifications',
            icon: Stack(
              children: <Widget>[
                FaIcon(FontAwesomeIcons.bell),
                Positioned(
                    top: 0,
                    right: 0,
                    //child: Icon(Icons.brightness_1, size: 8, color: Colors.redAccent))
                  child: BounceInDown(
                    from: 10,
                    animate: (numberNotify > 0)? true : false,
                    child: Bounce(
                      from: 10,
                      controller: (controller) => Provider.of<_NotificationModel>(context).bounceController = controller,
                      child: Container(
                        alignment: Alignment.center,
                        child: Text('$numberNotify', style: TextStyle(color: Colors.white, fontSize: 8)),
                        width: 12,
                        height: 12,
                        decoration: BoxDecoration(
                          color: Colors.redAccent,
                          shape: BoxShape.circle
                        ),
                      ),
                    ),
                  )),
              ],
            )),
        BottomNavigationBarItem(
            label: 'My Dog', icon: FaIcon(FontAwesomeIcons.dog))
      ],
    );
  }
}

class _BotonFlotante extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
        backgroundColor: Colors.pink,
        child: const FaIcon(FontAwesomeIcons.play),
        onPressed: () {
          final notyModel = Provider.of<_NotificationModel>(context, listen: false);
          int number = notyModel.number;
          number++;
          Provider.of<_NotificationModel>(context, listen: false).number = number;

          if(number >= 2){
            final controller = notyModel.bounceController;
            controller.forward(from: 0.0);
          }

        });
  }
}

class _NotificationModel extends ChangeNotifier{

  int _number = 0;
  late AnimationController _bounceController;

  int get number => _number;
  set number(int value){
    _number = value;
    notifyListeners();
  }

  AnimationController get bounceController => _bounceController;
  set bounceController(AnimationController controller){
    _bounceController = controller;
    notifyListeners();
  }

}
