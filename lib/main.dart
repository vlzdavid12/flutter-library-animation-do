import 'package:animate_flutter_do/pages/navegation_page.dart';
import 'package:animate_flutter_do/pages/page_one.dart';
import 'package:flutter/material.dart';
import 'package:animate_flutter_do/pages/twitter_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Animate do',
      home: PageOne(),
    );
  }
}




